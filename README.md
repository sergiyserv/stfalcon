composer install

php app/console doctrine:migrations:migrate

php app/console doctrine:fixtures:load

#API DOC ADDRESS

> /api/doc

# Bundle tests

> php bin/codecept run -c src/AppBundle