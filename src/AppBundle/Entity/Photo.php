<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Type;

/**
 * Photo
 *
 * @ORM\Table(name="photos")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PhotoRepository")
 * @Vich\Uploadable
 */
class Photo
{
    /**
     * @var integer
     *
     * @Groups({"list", "details"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var File
     * 
     * @Type("Symfony\Component\HttpFoundation\File\File")
     * @Groups({"doc"})
     * @Assert\NotBlank()
     * @Vich\UploadableField(mapping="photo", fileNameProperty="fileName")
     */
    private $file;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255)
     */
    private $fileName;

    /**
     * @var string
     * 
     * @Groups({"list", "details"})
     */
    private $fileFullPath;

    /**
     * @var ArrayCollection
     * 
     * @Groups({"list", "details"})
     * @ManyToMany(targetEntity="Tag")
     * @JoinTable(name="photos_tags",
     *      joinColumns={@JoinColumn(name="photo_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="tag_id", referencedColumnName="id")}
     *      )
     */
    private $tags;


    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get tags
     *
     * @return ArrayCollection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add tag
     *
     * @param Tag $tag
     *
     * @return Photo
     */
    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Get file
     * 
     * @return File|null
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|UploadedFile $file
     *
     * @return Photo
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file name
     * 
     * @return string|null
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set file name
     * 
     * @param string $fileName
     * @return Photo
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get file full path
     * 
     * @return string
     */
    public function getFileFullPath()
    {
        return $this->fileFullPath;
    }

    /**
     * Set file full path
     * 
     * @param string $fileFullPath
     * @return Photo
     */
    public function setFileFullPath($fileFullPath)
    {
        $this->fileFullPath = $fileFullPath;

        return $this;
    }
}
