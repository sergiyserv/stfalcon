<?php

namespace AppBundle\Entity;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use AppBundle\Entity\Photo;

class PhotoSubscriber implements EventSubscriber
{
    /**
     * @var string
     */
    private $photoPath;
    
    /**
     * @param string $photoPath injection
     */
    public function __construct($photoPath)
    {
        $this->photoPath = $photoPath;
    }

    /** 
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'postLoad',
            'postPersist',
            'postUpdate',
        ];
    }

    /**
     * Post load handler
     * 
     * @param LifecycleEventArgs $args
     */
    public function postLoad(LifecycleEventArgs $args)
    {
        $this->setFileFullPath($args);
    }

    /**
     * Post update handler
     * 
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->setFileFullPath($args);
    }

    /**
     * Post persist handler
     * 
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->setFileFullPath($args);
    }

    /**
     * It sets fileFullPath to the Photo entity
     * 
     * @param LifecycleEventArgs $args
     */
    protected function setFileFullPath(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof Photo) {
            return;
        }

        $entity->setFileFullPath($this->photoPath.$entity->getFileName());
    }
}
