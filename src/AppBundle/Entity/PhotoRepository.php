<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * PhotoRepository
 */
class PhotoRepository extends EntityRepository
{
    /**
     * Find by tag
     * 
     * @param string $query
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function findByTag($query = null, $offset = 0, $limit = 10)
    {
        $builder = $this->createQueryBuilder('p');
        $builder
            ->select('p')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
        ;
        
        if (!empty($query)) {
            $builder
                ->leftJoin('p.tags', 't')
                ->andWhere($builder->expr()->like('t.name', ":query"))
                ->setParameter('query', "%$query%")
            ;
        }

        return $builder->getQuery()->getResult(); 
    }
}
