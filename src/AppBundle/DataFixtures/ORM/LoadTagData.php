<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Tag;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadTagData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $tagSea = new Tag();
        $tagSea->setName('sea');
        $this->addReference('tag-sea', $tagSea);
        
        $tagOcean = new Tag();
        $tagOcean->setName('ocean');
        $this->addReference('tag-ocean', $tagOcean);
        
        $tagPhone = new Tag();
        $tagPhone->setName('phone');
        $this->addReference('tag-phone', $tagPhone);
        
        $tagCar = new Tag();
        $tagCar->setName('car');
        $this->addReference('tag-car', $tagCar);
        
        $tagUa = new Tag();
        $tagUa->setName('ua');
        $this->addReference('tag-ua', $tagUa);

        $manager->persist($tagSea);
        $manager->persist($tagOcean);
        $manager->persist($tagPhone);
        $manager->persist($tagCar);
        $manager->persist($tagUa);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 0; // the order in which fixtures will be loaded
    }
}
