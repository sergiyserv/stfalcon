<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Photo;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPhotoData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $tagSea = $this->getReference('tag-sea');
        $tagOcean = $this->getReference('tag-ocean');
        $tagPhone = $this->getReference('tag-phone');
        $tagCar = $this->getReference('tag-car');
        $tagUa = $this->getReference('tag-ua');
        
        $photo1 = new Photo();
        $photo1->setFileName('no_file1');
        $photo1->addTag($tagSea);
        $photo1->addTag($tagOcean);

        $photo2 = new Photo();
        $photo2->setFileName('no_file2');
        $photo2->addTag($tagSea);
        $photo2->addTag($tagCar);
        $photo2->addTag($tagUa);

        $photo3 = new Photo();
        $photo3->setFileName('no_file3');
        $photo3->addTag($tagPhone);
        $photo3->addTag($tagCar);

        $photo4 = new Photo();
        $photo4->setFileName('no_file4');
        $photo4->addTag($tagPhone);
        $photo4->addTag($tagUa);

        $manager->persist($photo1);
        $manager->persist($photo2);
        $manager->persist($photo3);
        $manager->persist($photo4);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}
