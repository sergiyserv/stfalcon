<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Photo;
use AppBundle\Entity\Tag;
use AppBundle\Form\PhotoFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as FOSAnnotations;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class PhotosController extends BaseRestController
{
    /**
     * FIXME: "output=" from ApiDoc not work properly
     * 
     * @ApiDoc(
     *  resource=true,
     *  description="Get list of photos",
     *  filters={
     *      {"name"="q", "dataType"="string"},
     *      {"name"="offset", "dataType"="integer"},
     *      {"name"="limit", "dataType"="integer"}
     *  },
     *  statusCodes={
     *      200="Returned when successful"
     *  }
     * )
     *
     * @FOSAnnotations\View(serializerGroups={"list"})
     * @param Request $request
     * @return View
     */
    public function getPhotosAction(Request $request)
    {
        $query = $request->query->get('q');
        $offset = $request->query->get('offset');
        $limit = $request->query->get('limit', 10);

        $em = $this->getDoctrine()->getManager();
        
        $photos = $em->getRepository('AppBundle:Photo')
                     ->findByTag($query, $offset , $limit);

        return $this->view($photos);
    }

    /**
     * @ApiDoc(
     *  description="Add photo",
     *  statusCodes={
     *      201="Returned when successful",
     *      400="Returned when validation error"
     *  },
     *  input={
     *      "class"="AppBundle\Entity\Photo",
     *      "groups"={"doc"}
     *  }
     * )
     *
     * @FOSAnnotations\View(serializerGroups={"details"})
     * @param Request $request
     * @return View
     */
    public function postPhotosAction(Request $request)
    {
        $form = $this->createForm(PhotoFormType::class);
        $form->handleRequest($request);
        if (!$form->isValid()) {
            return $this->formErrorsView($form);
        }

        $photo = $form->getData();
        $this->persistEntityAndFlushEM($photo);

        return $this->view($photo, 201);
    }

    /**
     * // TODO: refactor it
     *
     * @ApiDoc(
     *  description="Add tag to photo",
     *  statusCodes={
     *      200="Returned when successful",
     *      404={
     *        "Returned when the photo is not found",
     *        "Returned when the tag is not found"
     *      }
     *  }
     * )
     * 
     * @FOSAnnotations\View(serializerGroups={"details"})
     * @ParamConverter("photo", class="AppBundle:Photo")
     * @ParamConverter("tag", class="AppBundle:Tag") 
     * @param Photo $photo
     * @param Tag $tag
     * @return View
     */
    public function postPhotosTagAction(Photo $photo, Tag $tag)
    {
        $photo->addTag($tag);
        $this->persistEntityAndFlushEM($photo);

        return $this->view($photo);
    }

    /**
     * @ApiDoc(
     *  description="Delete photo",
     *  statusCodes={
     *      200="Returned when successful",
     *      404={
     *        "Returned when the photo is not found"
     *      }
     *  }
     * )
     *
     * @ParamConverter("photo", class="AppBundle:Photo")
     * @param Photo $photo
     * @return View
     */
    public function deletePhotosAction(Photo $photo)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($photo);
        $em->flush();

        return $this->view();
    }
}
