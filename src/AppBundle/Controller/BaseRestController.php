<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use FOS\RestBundle\View\View;

abstract class BaseRestController extends FOSRestController
{
    /**
     * It return form errors view
     *
     * @param Form $form
     * @return View
     */
    protected function formErrorsView(Form $form)
    {
        $errors = [];

        if ($form->isValid()) {
            return $errors;
        }

        $fields = $form->all();

        foreach ($fields as $field) {
            $fieldErrors = $field->getErrors(true);
            if (!$field->isValid() && count($fieldErrors) > 0) {
                $errors[$field->getName()] = $fieldErrors[0]->getMessage();
            }
        }

        if (count($errors) === 0) {
            $allErrors = $form->getErrors(true);
            foreach ($allErrors as $error) {
                if ($error instanceof FormError) {
                    $errors['other_errors'][] = $error->getMessage();
                }
            }
        }

        return $this->view($errors, 400);
    }

    /**
     * It persist entity and flush entity manager
     *
     * @param object $entity
     * @return void
     */
    protected function persistEntityAndFlushEM($entity)
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
    }
}
