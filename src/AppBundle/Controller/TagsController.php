<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Tag;
use AppBundle\Form\TagFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as FOSAnnotations;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class TagsController extends BaseRestController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get list of tags",
     *  statusCodes={
     *      200="Returned when successful"
     *  }
     * )
     *
     * @FOSAnnotations\View(serializerGroups={"list"})
     * @return View
     */
    public function getTagsAction()
    {
        $tags = $this->getDoctrine()
                     ->getRepository('AppBundle:Tag')
                     ->findAll();

        return $this->view($tags);
    }

    /**
     * @ApiDoc(
     *  description="Add tag",
     *  statusCodes={
     *      201="Returned when successful",
     *      400="Returned when validation error"
     *  },
     *  input={
     *      "class"="AppBundle\Entity\Tag",
     *      "groups"={"doc"}
     *  }
     * )
     *
     * @FOSAnnotations\View(serializerGroups={"details"})
     * @param Request $request
     * @return View
     */
    public function postTagsAction(Request $request)
    {
        $form = $this->createForm(TagFormType::class);
        // FIXME: $form->handleRequest($request); it not work
        $form->submit($request);
        if (!$form->isValid()) {
            /**
             FIXME:
             
             "return $form;" - it works incorrect

             it returns this
             {
                "children": {
                  "name": {
                    "errors": [
                      "This value is already used."
                    ]
                  }
                }
              }

             but should this @see http://symfony.com/doc/master/bundles/FOSRestBundle/2-the-view-layer.html#forms-and-views
             {
                "code": 400,
                "message": "Validation Failed";
                "errors": {
                  "children": {
                    "name": {
                      "errors": [
                        "This value should not be blank."
                      ]
                    }
                  }
                }
             */
            
            return $this->formErrorsView($form);
        }

        $tags = $form->getData();
        $this->persistEntityAndFlushEM($tags);

        return $this->view($tags, 201);
    }

    /**
     * @ApiDoc(
     *  description="Delete tag",
     *  statusCodes={
     *      200="Returned when successful",
     *      404={
     *        "Returned when the tag is not found"
     *      }
     *  }
     * )
     *
     * @ParamConverter("tag", class="AppBundle:Tag")
     * @param Tag $tag
     * @return View
     */
    public function deleteTagsAction(Tag $tag)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($tag);
        $em->flush();

        return $this->view();
    }
}
