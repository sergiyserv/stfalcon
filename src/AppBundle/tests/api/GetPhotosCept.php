<?php use AppBundle\ApiTester;

$I = new ApiTester($scenario);
$I->haveInRepository('AppBundle\Entity\Photo', ['fileName' => 'test_photo']);
$I->wantTo('Validate structure of get photos responses');
$I->haveHttpHeader('Content-Type', 'application/json');
$I->sendGET('/photos');
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
$I->seeResponseIsJson();
$I->seeResponseMatchesJsonType([
    'id' => 'integer',
    'file_full_path' => 'string',
    'tags' => 'array'
]);
