<?php use AppBundle\ApiTester;

$I = new ApiTester($scenario);

$record = ['name' => 'test_del'];
$tagId = $I->haveInRepository('AppBundle\Entity\Tag', $record);

$I->wantTo('Delete tag and see result');
$I->haveHttpHeader('Content-Type', 'application/json');
$I->seeInRepository('AppBundle\Entity\Tag', $record);
$I->sendDELETE('/tags/'.$tagId);
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::NO_CONTENT);
$I->dontSeeInRepository('AppBundle\Entity\Tag', $record);
