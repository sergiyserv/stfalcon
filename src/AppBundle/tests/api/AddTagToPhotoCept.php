<?php use AppBundle\ApiTester;

$I = new ApiTester($scenario);

$photoId = $I->haveInRepository('AppBundle\Entity\Photo',
                                ['fileName' => 'test_photo']);
$record = ['name' => 'test_add_tag'];
$tagId = $I->haveInRepository('AppBundle\Entity\Tag', $record);

$I->wantTo('Add tag to photo via API');
$I->haveHttpHeader('Content-Type', 'application/json');
$I->sendPOST('/photos/'.$photoId.'/tags/'.$tagId);
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
$I->seeResponseIsJson();
$I->seeResponseContainsJson($record);
