<?php use AppBundle\ApiTester;

$record = ['name' => 'test_tag'];

$I = new ApiTester($scenario);
$I->wantTo('Create a tag via API');
$I->haveHttpHeader('Content-Type', 'application/json');
$I->sendPOST('/tags', $record);
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::CREATED);
$I->seeResponseIsJson();
$I->seeResponseContainsJson($record);
$I->seeInRepository('AppBundle\Entity\Tag', $record);
