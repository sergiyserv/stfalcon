<?php use AppBundle\ApiTester;

$I = new ApiTester($scenario);
$I->haveInRepository('AppBundle\Entity\Tag', ['name' => 'test_tag']);
$I->wantTo('Validate structure of get tags responses');
$I->haveHttpHeader('Content-Type', 'application/json');
$I->sendGET('/tags');
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
$I->seeResponseIsJson();
$I->seeResponseMatchesJsonType([
    'id' => 'integer',
    'name' => 'string'
]);
