<?php use AppBundle\ApiTester;
$I = new ApiTester($scenario);

$I = new ApiTester($scenario);

$record = ['fileName' => 'photo_del'];
$photoId = $I->haveInRepository('AppBundle\Entity\Photo', $record);

$I->wantTo('Delete photo and see result');
$I->haveHttpHeader('Content-Type', 'application/json');
$I->seeInRepository('AppBundle\Entity\Photo', $record);
$I->sendDELETE('/photos/'.$photoId);
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::NO_CONTENT);
$I->dontSeeInRepository('AppBundle\Entity\Photo', $record);
