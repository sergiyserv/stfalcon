<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20161121020916 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $tags = $schema->createTable('tags');
        $tags->addOption('type', 'InnoDB');
        $tags->addOption('charset', 'utf8');
        $tags->addColumn('id', 'integer', ['length' => 11, 'notnull' => true, 'autoincrement' => true]);
        $tags->addColumn('name', 'string', ['length' => 255, 'notnull' => true]);
        $tags->setPrimaryKey(['id']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('tags');
    }
}
