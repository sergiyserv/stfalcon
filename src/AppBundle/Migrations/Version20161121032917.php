<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20161121032917 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $photosTags = $schema->createTable('photos_tags');
        $photosTags->addOption('type', 'InnoDB');
        $photosTags->addOption('charset', 'utf8');
        $photosTags->addColumn('photo_id', 'integer', ['length' => 11, 'notnull' => true]);
        $photosTags->addColumn('tag_id', 'integer', ['length' => 11, 'notnull' => true]);
        $photosTags->setPrimaryKey(['photo_id', 'tag_id']);
        
        $photosTags->addForeignKeyConstraint('photos', ['photo_id'], ['id'],
            ['onDelete'=>'CASCADE', 'onUpdate' => 'CASCADE'],
            'fk_photos_tags_photos');        
        $photosTags->addForeignKeyConstraint('tags', ['tag_id'], ['id'],
            ['onDelete'=>'CASCADE', 'onUpdate' => 'CASCADE'],
            'fk_photos_tags_tags');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $photosTags = $schema->getTable('photos_tags');
        $photosTags->removeForeignKey('fk_photos_tags_photos');
        $photosTags->removeForeignKey('fk_photos_tags_tags');

        $schema->dropTable('photos_tags');
    }
}
