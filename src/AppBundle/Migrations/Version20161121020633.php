<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20161121020633 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $photos = $schema->createTable('photos');
        $photos->addOption('type', 'InnoDB');
        $photos->addOption('charset', 'utf8');
        $photos->addColumn('id', 'integer', ['length' => 11, 'notnull' => true, 'autoincrement' => true]);
        $photos->addColumn('file_name', 'string', ['length' => 255, 'notnull' => true]);
        $photos->setPrimaryKey(['id']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('photos');
    }
}
